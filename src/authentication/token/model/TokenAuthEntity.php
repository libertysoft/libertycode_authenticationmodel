<?php
/**
 * This class allows to define token authentication entity class.
 * Token authentication entity allows to design an authentication entity class,
 * uses token attribute, as identification and authentication information.
 *
 * Token authentication entity uses the following specified configuration:
 * [
 *     Authentication entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\token\model;

use liberty_code\authentication_model\authentication\model\AuthenticationEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\authentication_model\authentication\token\library\ConstTokenAuth;



/**
 * @property string $strAttrToken
 */
class TokenAuthEntity extends AuthenticationEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute token
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstTokenAuth::ATTRIBUTE_KEY_TOKEN,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstTokenAuth::ATTRIBUTE_ALIAS_TOKEN,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            ConstTokenAuth::ATTRIBUTE_KEY_TOKEN => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ]
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabIdDataEngine()
    {
        // Return result
        return array(
            ConstTokenAuth::TAB_DATA_KEY_TOKEN => $this->getAttributeValue(ConstTokenAuth::ATTRIBUTE_KEY_TOKEN)
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthDataEngine()
    {
        // Return result
        return array(
            ConstTokenAuth::TAB_DATA_KEY_TOKEN => $this->getAttributeValue(ConstTokenAuth::ATTRIBUTE_KEY_TOKEN)
        );
    }



}