<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\token\library;



class ConstTokenAuth
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_TOKEN = 'strAttrToken';

    const ATTRIBUTE_ALIAS_TOKEN = 'token';

    // Data configuration
    const TAB_DATA_KEY_TOKEN = 'token';



}