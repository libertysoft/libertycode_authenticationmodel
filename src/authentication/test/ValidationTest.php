<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load external library test
require_once($strRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\validation\validator\api\ValidatorInterface;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);



// Init validator
$objPref = new Preference(array(
    'source' => ValidatorInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objValidator],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);


