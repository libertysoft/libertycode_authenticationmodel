<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/authentication/test/ValidationTest.php');

// Use
use liberty_code\authentication_model\authentication\secret\library\ConstSecretAuth;
use liberty_code\authentication_model\authentication\secret\model\SecretAuthEntity;
use liberty_code\authentication_model\authentication\token\library\ConstTokenAuth;
use liberty_code\authentication_model\authentication\token\model\TokenAuthEntity;
use liberty_code\authentication_model\authentication\credential\library\ConstCredentialAuth;
use liberty_code\authentication_model\authentication\credential\model\CredentialAuthEntity;



// Test secret authentication
$tabTabConfig = array(
    [
        ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER => 7,
        ConstSecretAuth::ATTRIBUTE_KEY_SECRET => 'pw'
    ], // Ko: Identifier bad format

    [
        ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER => 'user-1',
        ConstSecretAuth::ATTRIBUTE_KEY_SECRET => 7
    ], // Ko: Secret bad format

    [
        ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER => 'user-1',
        ConstSecretAuth::ATTRIBUTE_KEY_SECRET => 'pw'
    ], // Ok

    [
        ConstSecretAuth::ATTRIBUTE_KEY_SECRET => 'pw'
    ], // Ko: Identifier not found

    [
        ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER => 'user-1'
    ] // Ok
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test secret authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        /** @var SecretAuthEntity $objSecretAuth */
        $objSecretAuth = $objProvider->get(SecretAuthEntity::class);
        $objSecretAuth->setAuthConfig($tabConfig);

        // Set validation
        $objSecretAuth->setAttributeValid();

        echo('Get configuration: <pre>');var_dump($objSecretAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objSecretAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objSecretAuth->getTabAuthData());echo('</pre>');
        echo('Get identifier: <pre>');var_dump($objSecretAuth->strAttrIdentifier);echo('</pre>');
        echo('Get secret: <pre>');var_dump($objSecretAuth->strAttrSecret);echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test token authentication
$tabTabConfig = array(
    [
        ConstTokenAuth::ATTRIBUTE_KEY_TOKEN => 7,
    ], // Ko: Token bad format

    [
        ConstTokenAuth::ATTRIBUTE_KEY_TOKEN => 'token-1'
    ], // Ok

    [] // Ko: Token not found
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test token authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        /** @var TokenAuthEntity $objTokenAuth */
        $objTokenAuth = $objProvider->get(TokenAuthEntity::class);
        $objTokenAuth->setAuthConfig($tabConfig);

        // Set validation
        $objTokenAuth->setAttributeValid();

        echo('Get configuration: <pre>');var_dump($objTokenAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objTokenAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objTokenAuth->getTabAuthData());echo('</pre>');
        echo('Get token: <pre>');var_dump($objTokenAuth->strAttrToken);echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test credential authentication
$tabTabConfig = array(
    [
        ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL => 7,
    ], // Ko: Credential bad format

    [
        ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL => 'user-1'
    ], // Ok

    [], // Ko: Credential not found

    [
        ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL => 'user-1:pw'
    ], // Ok
);

foreach($tabTabConfig as $tabConfig)
{
    echo('Test credential authentication : <pre>');
    var_dump($tabConfig);
    echo('</pre>');

    try{
        /** @var CredentialAuthEntity $objCredentialAuth */
        $objCredentialAuth = $objProvider->get(CredentialAuthEntity::class);
        $objCredentialAuth->setAuthConfig($tabConfig);

        // Set validation
        $objCredentialAuth->setAttributeValid();

        echo('Get configuration: <pre>');var_dump($objCredentialAuth->getTabAuthConfig());echo('</pre>');
        echo('Get identification: <pre>');var_dump($objCredentialAuth->getTabIdData());echo('</pre>');
        echo('Get authentication: <pre>');var_dump($objCredentialAuth->getTabAuthData());echo('</pre>');
        echo('Get credential: <pre>');var_dump($objCredentialAuth->strAttrCredential);echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


