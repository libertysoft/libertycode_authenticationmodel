<?php
/**
 * This class allows to define secret authentication entity class.
 * Secret authentication entity allows to design an authentication entity class,
 * uses identifier attribute, as identification information
 * and secret attribute, as authentication information.
 *
 * Secret authentication entity uses the following specified configuration:
 * [
 *     Authentication entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\secret\model;

use liberty_code\authentication_model\authentication\model\AuthenticationEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\authentication_model\authentication\secret\library\ConstSecretAuth;



/**
 * @property string $strAttrIdentifier
 * @property null|string $strAttrSecret
 */
class SecretAuthEntity extends AuthenticationEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute identifier
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstSecretAuth::ATTRIBUTE_ALIAS_IDENTIFIER,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ],

            // Attribute secret
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstSecretAuth::ATTRIBUTE_KEY_SECRET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstSecretAuth::ATTRIBUTE_ALIAS_SECRET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ],
            ConstSecretAuth::ATTRIBUTE_KEY_SECRET => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-string' => ['type_string']
                        ],
                        'error_message_pattern' => '%1$s must be null or a string.'
                    ]
                ]
            ]
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabIdDataEngine()
    {
        // Return result
        return array(
            ConstSecretAuth::TAB_DATA_KEY_IDENTIFIER => $this->getAttributeValue(ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER)
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthDataEngine()
    {
        // Return result
        return array(
            ConstSecretAuth::TAB_DATA_KEY_SECRET => $this->getAttributeValue(ConstSecretAuth::ATTRIBUTE_KEY_SECRET)
        );
    }



}