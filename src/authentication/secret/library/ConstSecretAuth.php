<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\secret\library;



class ConstSecretAuth
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_IDENTIFIER = 'strAttrIdentifier';
    const ATTRIBUTE_KEY_SECRET = 'strAttrSecret';

    const ATTRIBUTE_ALIAS_IDENTIFIER = 'identifier';
    const ATTRIBUTE_ALIAS_SECRET = 'secret';

    // Data configuration
    const TAB_DATA_KEY_IDENTIFIER = 'identifier';
    const TAB_DATA_KEY_SECRET = 'secret';



}