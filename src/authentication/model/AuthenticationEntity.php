<?php
/**
 * This class allows to define authentication entity class.
 * Authentication entity allows to design an authentication class,
 * using entity.
 * Can be consider is base of all authentication entity types.
 *
 * Authentication entity uses the following specified configuration:
 * [
 *     @see AuthenticationEntity::hydrate():
 *         List of attributes hydration: @see AuthenticationEntity::getTabConfig()
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\authentication\authentication\api\AuthenticationInterface;



abstract class AuthenticationEntity extends ValidatorConfigEntity implements AuthenticationInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of identification data engine.
     *
     * @return mixed[]
     */
    abstract protected function getTabIdDataEngine();



    /**
     * Get specified identification formatted data.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getIdDataFormat($strKey, $value)
    {
        // Format by data key
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabIdData()
    {
        // Init var
        $result = $this->getTabIdDataEngine();
        array_walk(
            $result,
            function(&$value, $strKey) {
                $value = $this->getIdDataFormat($strKey, $value);
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get associative array of authentication data engine.
     *
     * @return mixed[]
     */
    abstract protected function getTabAuthDataEngine();



    /**
     * Get specified authentication formatted data.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAuthDataFormat($strKey, $value)
    {
        // Format by data key
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthData()
    {
        // Init var
        $result = $this->getTabAuthDataEngine();
        array_walk(
            $result,
            function(&$value, $strKey) {
                $value = $this->getAuthDataFormat($strKey, $value);
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get formatted authentication configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @return array
     */
    protected function getTabAuthConfigFormat(array $tabConfig)
    {
        // Return result
        return $tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthConfig()
    {
        // Init var
        $result = $this->getTabData(false);
        $result = $this->getTabAuthConfigFormat($result);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setAuthConfig(array $tabConfig)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfigFormat($tabConfig);

        // Hydrate attributes
        $this->hydrate($tabConfig);
    }



}