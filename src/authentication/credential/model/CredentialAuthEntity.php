<?php
/**
 * This class allows to define credential authentication entity class.
 * Credential authentication entity allows to design an authentication entity class,
 * uses credential attribute to retrieve identifier, as identification information
 * and secret, as authentication information.
 *
 * Credential authentication entity uses the following specified configuration:
 * [
 *     Authentication entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\credential\model;

use liberty_code\authentication_model\authentication\model\AuthenticationEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\authentication_model\authentication\credential\library\ConstCredentialAuth;



/**
 * @property string $strAttrCredential
 */
class CredentialAuthEntity extends AuthenticationEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute credential
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstCredentialAuth::ATTRIBUTE_ALIAS_CREDENTIAL,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ]
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get identifier.
     * Overwrite it to set specific feature.
     *
     * @return string
     */
    public function getStrIdentifier()
    {
        // Init var
        $tabMatch = array();
        $strPattern = '#^([^:]+):[^:]*$#';
        $strCredential = $this->getAttributeValue(ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL);
        $result = (
            (
                (preg_match($strPattern, $strCredential, $tabMatch) !== false) &&
                isset($tabMatch[1]) &&
                (trim($tabMatch[1]) != '')
            ) ?
                $tabMatch[1] :
                $strCredential
        );

        // Return result
        return $result;
    }



    /**
     * Get secret.
     * Overwrite it to set specific feature.
     *
     * @return null|string
     */
    public function getStrSecret()
    {
        // Init var
        $tabMatch = array();
        $strPattern = '#^[^:]+:([^:]*)$#';
        $strCredential = $this->getAttributeValue(ConstCredentialAuth::ATTRIBUTE_KEY_CREDENTIAL);
        $result = (
            (
                (preg_match($strPattern, $strCredential, $tabMatch) !== false) &&
                isset($tabMatch[1])
            ) ?
                $tabMatch[1] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabIdDataEngine()
    {
        // Return result
        return array(
            ConstCredentialAuth::TAB_DATA_KEY_IDENTIFIER => $this->getStrIdentifier()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabAuthDataEngine()
    {
        // Return result
        return array(
            ConstCredentialAuth::TAB_DATA_KEY_SECRET => $this->getStrSecret()
        );
    }



}