<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\authentication_model\authentication\credential\library;



class ConstCredentialAuth
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CREDENTIAL = 'strAttrCredential';

    const ATTRIBUTE_ALIAS_CREDENTIAL = 'credential';

    // Data configuration
    const TAB_DATA_KEY_IDENTIFIER = 'identifier';
    const TAB_DATA_KEY_SECRET = 'secret';



}