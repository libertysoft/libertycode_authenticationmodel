LibertyCode_AuthenticationModel
===============================



Description
-----------

Library contains authentication model components, 
to manage identification and authentication system.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
--------------------------------------------

1. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all standard rules, 
        added on its rule collection.
        
    - Validator rules implementation (or equivalent): 
    
        Validator provided on entities, 
        must contain all validator rules, 
        added on its rule collection.
        Each validator rule must use validator,
        with same implementation as validator provided on entities.

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/authentication_model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/authentication_model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Authentication

Authentication entity allows to design an authentication class,
using entity.

_Elements_

- AuthenticationEntity

    Extends authentication features. 
    Uses entity to design an authentication class, 
    to get all identification and authentication information,
    about one specific entity.

- SecretAuthEntity

    Extends authentication entity features.
    Uses identifier attribute, as identification information
    and secret attribute, as authentication information.

- TokenAuthEntity

    Extends authentication entity features.
    Uses token attribute, as identification and authentication information.

- CredentialAuthEntity

    Extends authentication entity features.
    Uses credential attribute to retrieve identifier, as identification 
    and secret, as authentication information.
    
_Example_

```php
// Get secret authentication entity
use liberty_code\authentication_model\authentication\secret\library\ConstSecretAuth;
use liberty_code\authentication_model\authentication\secret\model\SecretAuthEntity;
$secretAuthEntity = new SecretAuthEntity();
...
// Set identifier attribute
$secretAuthEntity->setAttributeValue(ConstSecretAuth::ATTRIBUTE_KEY_IDENTIFIER, '...');
...
// Set secret attribute
$secretAuthEntity->setAttributeValue(ConstSecretAuth::ATTRIBUTE_KEY_SECRET, '...');
...
// Get array of identification data
var_dump($secretAuthEntity->getTabIdData());
...
// Get array of authentication data
var_dump($secretAuthEntity->getTabAuthData());
...
```

---


