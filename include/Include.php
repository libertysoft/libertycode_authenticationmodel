<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/authentication/model/AuthenticationEntity.php');

include($strRootPath . '/src/authentication/secret/library/ConstSecretAuth.php');
include($strRootPath . '/src/authentication/secret/model/SecretAuthEntity.php');

include($strRootPath . '/src/authentication/token/library/ConstTokenAuth.php');
include($strRootPath . '/src/authentication/token/model/TokenAuthEntity.php');

include($strRootPath . '/src/authentication/credential/library/ConstCredentialAuth.php');
include($strRootPath . '/src/authentication/credential/model/CredentialAuthEntity.php');